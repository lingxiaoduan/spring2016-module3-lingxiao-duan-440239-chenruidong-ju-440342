<!DOCTYPE html>
<html>
    <head>
        <title>
            Show Story and Comments
        </title>
    </head>
    <body>
        <div id = main>
            <?php
            $replyToCommentCreator = "";
            $replyTo;
            
            function addComment($storyCreator,$replyTo){
                echo "<form action=\"submit_comment.php\" method=\"POST\">";
				echo "<input type=\"hidden\" name=\"token\" value=".htmlspecialchars($_SESSION['token']).">";
                echo "<p> Reply to:".htmlspecialchars($replyTo)."</p>";
                echo "<p> <textarea name=\"commentContent\" rows = 10 ></textarea> </p>";
                echo "<input  type=\"hidden\" name=\"replyCommentTo\" value = ".htmlspecialchars($replyTo).">";
                echo "<p> <input type=\"submit\" name=\"submitComment_button\" value=\"Submit\"> </p>";
                echo "</form>";
            }
			
			session_start();
			$user = $_SESSION['name'];
			$title = $_SESSION['title'];
			$mysqli = new mysqli("localhost", "wustl_inst", "wustl_pass", 'story');
			if($mysqli->connect_errno){
				printf("Connection failed: %s\n", $mysqli->connect_errno);
				exit;
			}
			$queryStory = $mysqli ->prepare("SELECT title, user, content, FROM story where title=?");
			if(!$queryStory){
				printf("Query all story list failed: %s\n", $mysqli ->connect_errno);
				exit;
			}
			$queryStory ->bind_param('s',$title);
			$queryStory ->execute();
			$queryStory ->bind_result($story_title, $author, $story_content);
			echo $story_title;

			if($queryStory ->fetch()){
				echo"<table>\n"; 
				echo "<tr>";
				printf("<td colspan=3> <h1>%s</h1> </td>",htmlspecialchars($story_title));
				echo "</tr>";
				echo "<tr>";
				printf("<td>creator: %s</td> <td>date: %s</td>",htmlspecialchars($author));
				echo "<td>";
				echo "<form action = \"display.php\" method=\"POST\">";
				echo "<input type=\"submit\" name=\"replyStoryButton\" value=\"Reply\">";
				echo "</form>";
				echo "</td>";
				echo "</tr>";
				echo"</table>\n";            
				echo "<p></p>";
				if ($story_content != "") {
					echo"<table>\n";
					echo "<tr>";
					printf("<td> <h3> %s </h3> </td>",htmlspecialchars($story_content));
					echo "</tr>";
					echo"</table>\n";
					echo "<p></p>";
				}
			}
			$queryStory->close;
			
			echo "<br><br><br><br>";
			
			
			
			
			$user = $_SESSION['name'];

			$queryComment = $connectComment ->prepare("SELECT count(*) FROM comment where story_id=?");
			if(!$queryComment){
				printf("Query all story list failed: %s\n", $connectComment ->connect_errno);
				exit;
			}
			$queryComment ->bind_param('i',$story_id);
			$queryComment ->execute();
			$queryComment ->bind_result($num);
			$queryComment ->fetch();
			$queryComment ->close();
			

			if (isset($_SESSION['name'])) {
				if(isset($_POST["replyCommentButton"])){
					$replyTo = htmlspecialchars($_POST["replyCommentTo"]);
				}elseif(isset($_POST["replyStoryButton"])){
					$replyTo = htmlspecialchars($_POST["replyStory"]);
				}else{
					$replyTo = $storyCreator;
				}
				addComment($storyCreator,$replyTo);
			} else {
				echo "<br>log in to reply<br><br>";
			}
            ?>
			
			<form action="main.php" >
				<input type="submit" name="returnStoryBoard" value="Back">
			</form>
        </div>
        
    </body>
</html>