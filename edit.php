<!DOCTYPE HTML>
<html>
	<head>
		<title> Editor </title>
	</head>
	
	<body><div>
	<?php
		session_start();
		if(isset($_POST['token']) && $_SESSION['token'] !== $_POST['token']){
			die("Failed");
		}
        if (isset($_POST['submit'])) {
			$title = htmlspecialchars($_SESSION['title']);
			$story = htmlspecialchars($_POST['story']);
			require 'connect.php';
			$stmt = $mysqli->prepare("update story set content=? where title=?");
			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$stmt->bind_param('ss',$story,$title);
			$stmt->execute();
			$stmt->close();
			header("Location: main.php");
			exit;
			
            }
    
	?>
	
	<form action="edit.php" method="POST">
		<?php
		//session_start();
		$title = $_SESSION['title'];
		$_SESSION['title'] = $title;
        require 'connect.php';
        $stmt = $mysqli->prepare("SELECT title, content FROM story where title=?");
        $stmt->bind_param('s', $title);
        $stmt->execute();
        $stmt->bind_result($tit, $content);
        $stmt->fetch();
        $stmt->close();
		
		//echo $title;
		//printf(htmlspecialchars($title));
		
		echo "
		<p>
			<label>Content:</label><br>
			<textarea name=\"story\" rows=4>".htmlspecialchars($content)."</textarea>
		</p>"
		?>
		<p>
			<input type="submit" name = "submit" value = "submit" />
		</p>
	</form>
	</div></body>
</html>
