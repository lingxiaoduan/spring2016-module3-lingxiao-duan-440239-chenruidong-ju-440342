<?php
	require 'connect.php';
	$user = htmlentities($_POST['user']);
	$password = htmlentities($_POST['password']);

    if (isset($_POST['reg'])) {
        header("Location: reg.html");
        exit;
    }
	if (isset($_POST['guest'])) {
        session_start();
		$_SESSION['name'] = 'guest';
        header("Location: main.php");
        exit;
    }
	if ($user != "" && isset($user) && isset($password) && $password != "") {
        if (!preg_match('/^[\w_\-]+$/', $_POST['user'])) {
            echo "Invalid username";
            exit;
        }
		$stmt = $mysqli->prepare("SELECT COUNT(*), username, password FROM authen WHERE username=?");
		 
		// Bind the parameter
		$stmt->bind_param('s', $user);
		$stmt->execute();
		 
		// Bind the results
		$stmt->bind_result($cnt, $user_id, $pwd_hash);
		$stmt->fetch();
		$pwd_guess = $_POST['password'];
		// Compare the submitted password to the actual password hash
		//echo "here";
		//echo crypt($password,$pwd_hash);
		//echo $pwd_hash;
		
		if( $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
			// Login succeeded!
            session_start();
			$_SESSION['name'] = $user;
			// Redirect to your target page
			header("Location: main.php");
            exit;
		}else{
			// Login failed; redirect back to the login screen
			header("Location: login.html");
			exit;
			//echo "not working";
		}
    }


?>