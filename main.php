<!DOCTYPE HTML>
<html>
    <head>
    <title> StoryList </title>
	<style type="text/css">
	h1 {
    font-size: 20px;
}
</style>
    </head>
	<body>

	<div>
	<?php
		session_start();
		$name = $_SESSION['name'];
		require('connect.php');
		$stmt = $mysqli->prepare("SELECT title, user FROM story");
		if(!$stmt){
			printf("Query all story list failed: %s\n", $mysqli->connect_error);
			exit;
		}
		$stmt->execute();
		$stmt->bind_result($title, $user);
		echo"<table style='width:100%'>\n";
		while($stmt->fetch()){
			echo "<tr>";
			printf("<td colspan=2><h1>%s</h1></td></tr> <tr> <td>%s</td></tr>",
				   htmlspecialchars($title),
				   htmlspecialchars($user));
			echo "<tr><td colspan=2>";
			echo "<form action = \"option.php\" method=\"POST\">";
			echo "<input type=\"hidden\" name=\"title\" value = ".htmlspecialchars($title).">";
			echo "<input type=\"submit\" name=\"read\" value=\"Read\">";
			
			if (isset($_SESSION['name']) &&
				htmlspecialchars($user) == $_SESSION['name']) {
				echo "<input type=\"submit\" name=\"edit\" value=\"edit\">";
				$_SESSION['title'] = $title;
				echo "<input type=\"submit\" name=\"delete\" value=\"delete\">";
			}
			echo "</form>";
			echo"</td></tr>";
			
		}
	?>
		<div>
		<!-- new story-->
		<?php
		//session_start();
		if (isset($_SESSION['name']) && $_SESSION['name']!== 'guest') {
			echo "<form action = \"new.php\">";
			echo "   <input type=\"submit\" value=\"Create a New Story\">";
			echo "</form>";
		} else {
			echo "<label>log in to create your story</label>";
		}
		?>
	</div>
	</div>
	<?php
	echo "you are logged in as "; echo $name;
	?>
	<p>
	<form action = "logoff.php">
	<input type = "submit" value = "Logoff">
	</form>
	</p>
	</body>
</html>