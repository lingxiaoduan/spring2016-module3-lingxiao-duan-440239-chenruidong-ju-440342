<?php
    session_start();
	$_SESSION['title'] = htmlspecialchars($_POST['title']);
    if (isset($_POST['read'])) {
        header("Location: display.php");
		exit;
		} 
	else if (isset($_POST['edit'])) {
        header("Location: edit.php");
		exit;
		} 
	else if (isset($_POST['delete'])) {
		//session_start();
        require 'connect.php';
		$stmt = $mysqli->prepare("delete from story where title=?");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$stmt->bind_param('s', $_SESSION['title']);
		$stmt->execute();
		$stmt->close();
		header("Location: main.php");
		exit;
		}
	else {
		header("Location: main.php");
		exit;
		}
?>