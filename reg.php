
<?php
	if (isset($_POST['back'])) {
		header("Location: login.html");
		exit;
	}
	if (isset($_POST['newuser']) && isset($_POST['newpassword']) && isset($_POST['dup'])) {
		// echo $_POST['newuser'];
		$userName = htmlentities($_POST['newuser']);
		$password = htmlentities($_POST['newpassword']);
		$confirm = htmlentities($_POST['dup']);
		if ($userName == "") {
			echo "Username cannot be blank!";
		} else if (!preg_match('/^[\w_\-]+$/', $userName)) {
			echo "Invalid username";
		} else if ($password == ""){
			echo "Password cannot be blank!";
		} else if ($password != $confirm) {
			echo "Passwords you entered don't match!";
		} else {
			require 'connect.php';
			$stmt = $mysqli->prepare("SELECT COUNT(*) FROM authen WHERE username=?");
			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$stmt->bind_param('s', $userName);
			$stmt->execute();
			$stmt->bind_result($cnt);
			$stmt->fetch();
			$stmt->close();
			// check if the username is already used
			if($cnt == 1) {
				echo "The username you entered has already been used!";
			} else {
				$stmt = $mysqli->prepare("insert into authen (username, password) values (?, ?)");
				if(!$stmt){
					printf("Query Prep Failed: %s\n", $mysqli->error);
					exit;
				}
				$stmt->bind_param('ss', $userName, crypt($password));
				$stmt->execute();
				$stmt->close();
				session_start();
				$_SESSION['name'] = $userName;
				$_SESSION['tag'] = substr(md5(rand()), 0, 10);
				header("Location: main.php");
				exit;
			}
		}
	}
?>